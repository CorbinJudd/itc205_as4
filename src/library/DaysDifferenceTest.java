package library;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

import library.entities.Calendar;

public class DaysDifferenceTest {

    @Test
    public void testDaysDifference() {
        //get initial date
        Date startDate = Calendar.getInstance().getDate();

        //increment date by 3 days
        Calendar.getInstance().incrementDate(3);

        //check if Calendar.getDaysDifference() produces correct result
        assertEquals("Difference was not 3 days", 3L, Calendar.getInstance().getDaysDifference(startDate));
    }
    
}
