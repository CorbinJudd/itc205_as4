package library;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

import library.entities.Calendar;
import library.entities.IBook;
import library.entities.ILibrary;
import library.entities.ILoan;
import library.entities.IPatron;
import library.entities.Library;
import library.entities.helpers.BookHelper;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

public class Bug1Test {

    @Test
    public void bugTest() {
        IBookHelper bookHelper = new BookHelper();
        IPatronHelper patronHelper = new PatronHelper();
        ILoanHelper loanHelper = new LoanHelper();

        ILibrary library = new Library(bookHelper, patronHelper, loanHelper);

        IBook book = library.addBook("author", "title", "02");
        IPatron patron = library.addPatron("Last", "first", "email", 1234);

        ILoan loan = library.issueLoan(book, patron);
        library.commitLoan(loan);

        //iincrement date fails?
        Calendar.getInstance().incrementDate(3);
        Date date = Calendar.getInstance().getDate();

        loan.updateOverDueStatus(date);

        library.dischargeLoan(loan, false);

        assertEquals("Fine was not generated", 1.0, patron.getFinesPayable(), 0.0);

        library.payFine(patron, 1.0);

    }
    
}
