package library;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

import library.entities.Calendar;
import library.entities.IBook;
import library.entities.ILibrary;
import library.entities.ILoan;
import library.entities.IPatron;
import library.entities.Library;
import library.entities.helpers.BookHelper;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

public class ThreeDayOverdueTest {
    
    @Test
    public void threeDayTest() {
        IBookHelper bookHelper = new BookHelper();
        IPatronHelper patronHelper = new PatronHelper();
        ILoanHelper loanHelper = new LoanHelper();

        ILibrary library = new Library(bookHelper, patronHelper, loanHelper);

        IPatron patron = library.addPatron("lastName", "firstName", "email", 700);
        IBook book = library.addBook("author", "title", "callNumber");

        ILoan loan = library.issueLoan(book, patron);

        library.commitLoan(loan);

        Calendar.getInstance().incrementDate(5);

        Date date = Calendar.getInstance().getDate();

        loan.updateOverDueStatus(date);

        assertEquals("Fine was not expected value", 3.0, library.calculateOverDueFine(loan), 0.0);

        library.dischargeLoan(loan, false);
        library.payFine(patron, 3.0);
    }
}
